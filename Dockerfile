FROM ubuntu
LABEL maintainer="Santucho Jonathan - ISTEA"
LABEL version="1"
LABEL description="Ejercicio WEB + BD"

#Comando para no intervenir en el build
ARG DEBIAN_FRONTEND=noninteractive

# Comando para descargar repositorios necesarios para la instalacion de LAMP
RUN apt update && apt install -y vim apache2 php libapache2-mod-php php-mysql php-curl php-zip && apt autoremove && apt cle$

#Copia de web
COPY startbootstrap-sb-admin-2 /var/www/html

CMD apachectl -DFOREGROUND